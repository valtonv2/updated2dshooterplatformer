**Disclaimer:**

The project was built some time ago, when I only had half a year of programming experience. This is why the project contains messy code, antipatterns and other bad ideas.

An improved version was created for the Programming Studio 2 model project. That repository is private. This version has a slightly larger feature set. These reasons are why this version was shared.

**Features:**
- Player character I designed from scratch.
- small animations
- HP
- movement left and right plus jumping
- ability to shoot in any direction
- ability to slow time and jump on top of projectiles
- ability to use energy shield and reflect incoming projectiles
- two enemy types
- Levels
    - created from tiles that are textured
    - built based on a color-coded image
- Menus

**Largest bugs:**
- On some systems the game may hang on menu change. If this happens, try moving the program window. 
- Changing game volume setting does not affect volume immediately.
- If the player hits the ground fast, it may sink slightly into the ground.

**How to run:**

Run command java -jar DWA.jar inside the project directory.

Screenshots:

<img width="801" alt="Näyttökuva 2019-3-14 kello 9 23 20" src="https://user-images.githubusercontent.com/43828011/54338734-64d1da00-463b-11e9-8b7b-73883ec10006.png">

<img width="801" alt="Näyttökuva 2019-3-14 kello 9 24 12" src="https://user-images.githubusercontent.com/43828011/54338840-b5493780-463b-11e9-887d-b484679aaac5.png">

<img width="798" alt="Näyttökuva 2019-3-14 kello 9 24 34" src="https://user-images.githubusercontent.com/43828011/54338916-ee81a780-463b-11e9-9b5d-7e2be4936c22.png">


